# La programacion logica y funcional

Un párrafo lorem ipsum dolor sit amet, consectetur adipiscing elit.
Integer tristique sit amet est eu vehicula. Vestibulum mollis pharetra
ultrices. Pellentesque tincidunt vestibulum elit, quis commodo arcu
imperdiet nec. Etiam ut sapien in erat porta eleifend ut a nunc.
Nulla facilisi. Maecenas eget nisl nec sem rutrum gravida.
Morbi euismod velit ultrices aliquam malesuada. 

## Conceptos Fundamentales

Otro párrafo lorem ipsum dolor sit amet, consectetur adipiscing elit.
Integer tristique sit amet est eu vehicula. Vestibulum mollis pharetra
ultrices. Pellentesque tincidunt vestibulum elit, quis commodo arcu
imperdiet nec. Etiam ut sapien in erat porta eleifend ut a nunc.
Nulla facilisi. Maecenas eget nisl nec sem rutrum gravida.
Morbi euismod velit ultrices aliquam malesuada. 

## Modelo de Programacion Funcional

Una lista sin enumeración:

- Tipo de datos.
- Funciones.
- Operadores.
- Aplicacion de las listas.

Una lista con enumeración:

1. Repaso de la lógica de primer orden.
2. Unificación y resolución.
3. Cláusulas de Horn, resolución SLD.
4. Programación lógica con cláusulas de Horn.

## Codigo Funcional 

Código fuente:

    class HolaMundo {
        public static void main(String[] args) {
            System.out.println("¡Hola, Mundo!"); 
        }
    }

Imagen en formato JPG:

![Una imagen](imagenes/una-imagen.jpg)

Un enlace:

[LuisGonzalezKevinIvan @ GitLab](https://gitlab.com/LuisGonzalezKevinIvan)